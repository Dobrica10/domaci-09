-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 13, 2018 at 06:42 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `domaci-09`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(100) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `running_time` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_genre` (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `movie_name`, `id_genre`, `running_time`, `description`) VALUES
(1, 'Lord of the rings', 4, '188 minutes', 'Fantasy adventure based on the novel of JR Tolkien. '),
(2, 'Mortedecai', 2, '120 minutes', 'Johnny Depp is a con artist with a taste in pieces of art.    '),
(3, 'Coco', 3, '89 minutes', ' Movie exploring Mexican culture and their tradition focusing on the Day of the dead(Dia de muertos). '),
(4, 'The Bride', 5, '95 minutes', 'Horror movie done in Russian production, directing wasn\'t as bad, but the overall story and acting was terrible. Avoid at all costs.      '),
(5, 'Thor: Ragnarok', 4, '130 minutes', 'Thor is imprisoned on the planet Sakaar, and must race against time to return to Asgard and stop Ragnarök,the destruction of his world, which is at the hands of the powerful and ruthless villain Hela.'),
(6, 'Commando', 1, '122 minutes', 'Arnold Schwarzenegger is a commando whose daughter has been kidnapped, what more do you need to know?');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(100) NOT NULL,
  `genre_description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre_name`, `genre_description`) VALUES
(1, 'action', 'Action film is a genre in which the protagonist or protagonists are thrust into a series of challenges that typically include violence, extended fighting, physical feats, and frantic chases.'),
(2, 'comedy', 'Comedy is a genre with light-hearted, humorous plotlines, centered on romantic ideals such as that true love is able to surmount most obstacles'),
(3, 'animated', 'An animated cartoon is a film for the cinema, television or computer screen, which is made using sequential drawings.'),
(4, 'adventure', 'Adventure films are a genre of film that typically use their action scenes to display and explore exotic locations in an energetic way.'),
(5, 'horror', 'A horror film is a movie that seeks to elicit a physiological reaction, such as an elevated heartbeat, through the use of fear and shocking one’s audiences.');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_film` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_film` (`id_film`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `id_film`, `date`, `time`, `location`) VALUES
(1, 6, '2018-04-17', '19:00:00', 'AMC'),
(2, 2, '2018-04-11', '19:15:00', 'Cinestar'),
(3, 4, '2018-04-12', '19:30:00', 'Arena Cineplex'),
(4, 3, '2018-04-20', '22:00:00', 'Odeon'),
(5, 5, '2018-04-09', '19:00:00', 'Lifka'),
(6, 1, '2018-04-11', '17:00:00', 'Radnicki');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `id_genre` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `id_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
